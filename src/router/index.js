import Vue from 'vue'
import VueRouter from 'vue-router'

import FormularioTabla from '../views/FormularioTabla'
import Baraja from '../components/Baraja'

Vue.use(VueRouter)

const routes = [{
        path: '/formulario-tabla',
        name: 'FormularioTabla',
        component: FormularioTabla
    },
    {
        path: '/baraja',
        name: 'Baraja',
        component: Baraja
    }
]

const router = new VueRouter({
    routes
})

export default router